class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :username,:website,:sex,:phone,:introduction])
  end

  def after_sign_in_path_for(resource)
     following_posts_user_path(resource)
  end

  def after_sign_out_path_for(resource)
    root_path # ログアウト後に遷移するpathを設定
  end
  
  before_action :set_search

  def set_search
  #@search = Post.search(params[:q])
  @search = Post.ransack(params[:q]) #ransackメソッド推奨
  @search_posts = @search.result.page(params[:page])
  end
end

