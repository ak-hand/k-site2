class UsersController < ApplicationController

  before_action :logged_in_user, only: [:edit, :updatec]
  before_action :correct_user,   only: [:edit, :update]



  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user,   only: [:edit, :update,:following,:follwer]



  def index
    @users = User.all
  end

  def following_posts
   @user  = User.find(params[:id])
   @users = @user.followings
      @posts = Post.limit(42).includes(:photos, :user).order('created_at DESC')
  end

  def new
  end
  
  
  def show
    @user = User.find(params[:id])
  end
  
  
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def following
    @user  = User.find(params[:id])
    @users = @user.followings
    render 'show_follow'
  end

  def followers
    @user  = User.find(params[:id])
    @users = @user.followers
    render 'show_follower'
  end


  def following_posts
    @user  = User.find(params[:id])
    @users = @user.followings
    @posts = Post.limit(30).includes(:photos, :user).order('created_at DESC')
  end

end