Rails.application.routes.draw do

  get 'password_resets/new'

  get 'password_resets/edit'

  devise_for :users
    
    
    
    
    
  root 'pages#home'
  get  '/help',    to: 'pages#help'
  get  '/about',   to: 'pages#about'
  get  '/contact', to: 'pages#contact'
  get  '/signup',  to: 'users#new'
  get  '/users/:id', to: 'users#show', as: 'user'
  get  '/users', to: 'users#index'
  get  '/terms/show'
  get  '/users/:id/following_posts',to: 'users#following_posts'
  post '/follow/:id' => 'relationships#follow', as: 'follow' # フォローする
  post '/unfollow/:id' => 'relationships#unfollow', as: 'unfollow' # フォロー外す

  resources :notifications, only: :index
  resources :users do
    member do
     get :following, :followers, :following_posts
    end
  end
  resources :relationships,       only: [:create, :destroy]
  
  resources :posts, only: %i(new create index show destroy) do
  resources :photos, only: %i(create)
  resources :users, only: :show
  resources :likes, only: %i(create destroy)
  resources :comments, only: %i(create destroy)
  resources :password_resets, only: [:new, :create, :edit, :update]
  end
  
end
